# ContextuWall#

The ContextuWall Server and the OmegaLib implementation of the ContextuWall Display.

## License ##
All contained projects are Copyright Monash University, 2016 licensed under the [MIT License](https://opensource.org/licenses/MIT).

### System Requirements ###
Versions tested against and download locations in parentheses.

##### Server #####
* node (4+; http://nodejs.org/download/). 
* npm (2.+; http://blog.npmjs.org/post/85484771375/how-to-install-npm)

##### Display (OmegaLib) #####
* A Linux Distribution (openSUSE recommended)
* C++ compiler (4.7.2)
* CMake (2.8.10.2)
* OmegaLib (4.2.5; https://github.com/uic-evl/omegalib/wiki/Building)

##### Display (Unity) #####
* Windows (8.1; works on Linux too, but untested)
* Unity (5.0.1f1; 64-bit)

NOTE: The Unity build works only on single node systems!

##### Display (SAGE2) #####
* Compatible with Linux/Windows/MacOSX running node.js

### Installation ###
* 'Master' branch contains latest stable release.
* 'Aureolin' branch contains latest _usually stable_ dev release.

##### Server #####
1. Under a new console tab navigate to ContextuWallServer/
2. Install all necessary packages with `npm install`
3. Run 'node ContextuWallServer.js'

To connect this server to an existing server, run 'node ContextuWallServer.js -s <server to connect to>'. Note that the server that is to be connected to must be running at the time this command is executed.

To connect to a SAGE2 instance provide the following parameters
```
-d <IP/DNS of SAGE2 server>
-m The port number for secured client connection
-n The port number for webUI connections
```
The port numbers can be found in the active SAGE2 configuration file on top
```
...
	name: "Server name",
	host: "192.168.1.1",
	port: 9292,
	secure_port: 9090,
...
```
where the secured client connection port is the `secure_port` and the webUI connection port is the `port`.


##### Display (OmegaLib) #####
1. Under a new console tab navigate to ContextuWallApp/build/
2. Build the application:
    1. Run './execute.sh' to build and immediately start up the application.
    2. Alternatively, build the application manually, with 'cmake' followed by 'make'
3. Once built, the application can be run with the command './ContextuWall'
4. If a crash occurs (happens when a node dies), or the application does not exit properly, run './Contextuwall -K' to ensure the application is terminated on all nodes, before attempting to run it anew.
5. Note that if running on a new display system, the display dimensions should be provided in the relevant 'ContextuWall.settings.json.<hostname>' file, else the default CAVE2 ones will be used.

##### Display (Unity) #####
1. Build the application:
    1. Navigate to the directory ContextuWallAppUnity/Assets/
    2. Open the file ContextuWall.unity in Unity
    3. Go to File->Build Settings->Build, ensuring the architecture is set to x86_64
    4. Save the executable at ContextuWallAppUnity/bin/ContextuWall.exe
2. Run it:
	1. Navigate to the directory ContextuWallAppUnity/bin/
	2. Run ContextuWall.exe

##### Display (SAGE2) #####
* Install SAGE2 by following the installation instructions [here](http://sage2.sagecommons.org/v1-release/)
* Download the ContextuWall SAGE2 application by cloning the repository from [here](https://bitbucket.org/monashsage2apps/contextuwall/) 
* From that repository copy the `contextuwall` folder into `SAGE2_ROOT_FOLDER/public/upload/apps/`
* (Re)start the SAGE2 server
* Open a browser and connect to the SAGE2 server

### Additional Information ###

* It does not matter whether the Server or the Display is run first. If the Display has no server to connect to at startup it'll poll periodically until it is able to connect. Furthermore, if the server crashes or is shut down while the display is running, the OmegaLib Display will return to its polling state and reconnect to the server automatically once it comes online again. The Unity Display will need to be restarted though.
* A start has been made at a ContextuWall Display implementation written purely in OmegaLib's Python API and can be found under ContextuWallAppPy/. It is currently on hold due to this API not exposing sufficient control at this stage.
* If connection problems arise, ensure that TCP ports 3000 and 3001 are open for inbound traffic on the head node, to allow connection for the surface client and display, respectively.
* To run the software on Monash's CAVE2 you need to switch to user 'dev' (password: cave2015), and you need to run both the server and the display application from /data/big/dev/IA/contextuwall/. Ask Toan or Owen if you need help.

### Contacts

* Email me at matthias.klapperstueck@monash.edu