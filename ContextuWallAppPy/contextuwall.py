import json
import asyncore, socket
import logging
from time import sleep

from command_listener import CommandListener
from inbox_watcher import InboxWatcher

# Set up a nice logging format.
logging.basicConfig(level=logging.DEBUG,
                    #format='---%(module)-20s %(threadName)-12s %(message)s',
                    format='---%(module)-20s %(message)s',
                    )

SERVER_SETTINGS = 'build/ContextuWall.settings.json'

class ContextuWall():
    # These are set by the CommandListener thread.
    incomingImageCommand = ''
    updateImageLocationCommand = ''
    clearWall = False
    
    def __init__(self, headHostname):
        self.headHostname = headHostname
        self.uiModule = UiModule.createAndInitialize()
        ui = self.uiModule.getUi()

        # Little title in top right corner of display.
        self.headerLabel = Label.create(ui)
        self.headerLabel.setFont('fonts/arial.ttf 30')
        self.headerLabel.setText('ContextuWall: %s' % self.headHostname)

        # Load in server configuration settings (see if there is a custom one).
        # NOTE: Child nodes only need to read this because they need to know the display dimensions,
        #  which omegalib already knows, but doesn't like to divulge.. Once we can extract these
        #  dimensions from omegalib, this whole settings file section can be changed to be read in
        #  exclusively by the head node.
        try:
            self.serverSettings = json.load(open(SERVER_SETTINGS + '.' + self.headHostname))
            logging.info('Using custom server settings: %s' % (SERVER_SETTINGS + '.' + self.headHostname))
        except IOError:
            self.serverSettings = json.load(open(SERVER_SETTINGS))
            logging.info('Using default server settings: %s' % SERVER_SETTINGS)
        # If no commandServerIP provided, assume localhost.
        if 'commandServerIP' not in self.serverSettings or self.serverSettings['commandServerIP'] == '':
            self.serverSettings['commandServerIP'] = 'localhost'
        
        if isMaster():
            #self.inboxWatcher = InboxWatcher(self)
            #self.inboxWatcher.addPath(self.serverSettings['contextuWallInboxPath'])
            self.commandListener = CommandListener(self)

# Run Contextuwall!
if isMaster():
    broadcastCommand("app = ContextuWall('%s')" % socket.gethostname())
    #queueCommand("app.headerLabel.setText('FOOBAR')")
    #queueCommand('sleep(5)')
    queueCommand('asyncore.loop()') #NOTE: This blocks all other processing, and not even just on head node. ;/
    