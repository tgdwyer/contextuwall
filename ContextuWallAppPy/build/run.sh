#!/bin/bash

# Comment this out to use default OMEGA_HOME directory.
#OMEGA_HOME=/home/jwglo1/dev/omegalib/build          #v6.0-beta6     personal build

WORKING_DIR=`pwd`
cd $OMEGA_HOME/bin

if [ $# -eq 0 ]
then
  orun -s $WORKING_DIR/../contextuwall.py
else
  orun -s $WORKING_DIR/$1
fi