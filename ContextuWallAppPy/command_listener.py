from threading import Thread
import asyncore, socket
from time import sleep
import logging

BUFFER_SIZE = 256

# TODO: Implement use of this fancy dict mapping.
COMMANDS = {'$incoming-image':'incomingImageCommand',
            '$update-image-location':'updateImageLocationCommand',
            '$clear-wall':'clearWall'
            }

class CommandListener(asyncore.dispatcher):
    def __init__(self, app):
        asyncore.dispatcher.__init__(self)
        self.app = app
        
        # Create the socket.
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect((app.serverSettings['commandServerIP'], app.serverSettings['commandServerPort']))
        
        # Send display specs before commencing listening cycle.
        # TODO: Determine how to extract display dimensions from omegalib...
        self.send('$display-specs$%s$%i$%i' %(self.app.headHostname,
                                              self.app.serverSettings['screenWidth'],
                                              self.app.serverSettings['screenHeight']))
    def handle_connect(self):
        logging.debug('Connection with server established.')
    
    def handle_close(self):
        logging.debug('Server closed the socket.')
        self.app.clearWall = True
        self.close()

    def handle_read(self):
        msg = self.recv(BUFFER_SIZE)
        logging.debug('Received message: %s' % msg)
        #if msg.startswith('$incoming-image'): self.app.incomingImageCommand = msg
        #elif msg.startswith('$update-image-location'): self.app.updateImageLocationCommand = msg
        #elif msg.startswith('$clear-wall'): self.app.clearWall = True
        
        
    

#logging.debug('Connection error: ' + str(e) + '. Trying again in 5s.')
#sleep(5)