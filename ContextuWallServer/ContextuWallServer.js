// Argument parser:
//  "_" is an array of unnamed arguments in the order they're provided
//  all single character keys are arguments provided with a single character key (ie. "-s <myArgument>"
//  all string keys are arguments provided with string keys (ie. "--server <myArgument")
// NOTE: Single character kets use a single preceding hyphen, while string keys use a double hyphen, as is the convention in most programs.
var argv = require('minimist')(process.argv.slice(2))
var ip = argv._[0] || "130.194.70.46"
var port = argv.port || argv.p || argv._[1] || 3000
var remoteServer = argv.server || argv.s

var fileSavePath
// Require the libraries:
var SocketIOFileUploadServer = require('socketio-file-upload')
var socketio = require('socket.io')
var doublelist = require('doublelist')
var http = require('http')
var express = require('express')
var fs = require('fs')
var net = require('net')
var app = express()
var server = http.createServer(app)

// Contains the socket to the display running on the local machine. (NET.SOCKET)
GLOBAL.LOCAL_DISPLAY = null

// Contains the socket to a 'parent' server that this server reports changes to. (SOCKET.IO)
GLOBAL.PARENT_SERVER = null

// This construct is simply to be able to provide proper connection messages.
//  The inbuilt socket.io counts aren't reliable during connection/disconnection events (race condition stuff).
GLOBAL.NUM_CONNECTIONS = 0

// Contains all of the data pertaining to what is to be displayed on the displays and clients.
// The displays and clients are able to rebuild their versions of the model from this one when they reconnect.
GLOBAL.MODEL = {}
MODEL.images = new doublelist()
MODEL.displaySpecs = new doublelist()

app.set('port', port)
app.use(SocketIOFileUploadServer.router)

server.listen(port, ip, function() {
    fileSavePath= __dirname + "/inbox"
    console.log("Local ContextuWall server running on: " + ip + ":" + port)
    console.log("fileSavePath= " + fileSavePath)
})

app.get('/portal', function (req, res) {
    res.sendfile(__dirname + '/portal.html')
})

// A temp logging construct while i ceebs finding and installing an npm module to do it.
DEBUG = true
function debugMsg(msg){
    if(DEBUG){
        console.log('---DEBUG MESSAGE: ' + msg)
    }
}

//*************************************************************
//***** NET.SOCKET SERVER SETUP FOR DISPLAY COMMUNICATION *****
//*************************************************************
net.createServer(function (socket) {
    if(LOCAL_DISPLAY === null){
        LOCAL_DISPLAY = socket
        console.log('Display connected.')
        sendMessageToDisplay('Welcome')
        socket.on('error', console.error)

        //Add a 'data' event handler for the display socket.
        // <data> is what the display has sent to this socket.
        socket.on('data', function(data) {
            //TODO: Change to expext a byte at the start, indicating the length of the message.
            console.log('DATA: ' + data)
            var packet = ('' + data).split('$')
            if(packet[1] !== 'display-specs') return
            var specs = {name: packet[2], width: packet[3], height: packet[4]}
            if (MODEL.displaySpecs.hasKey(specs.name)) {
                console.log("---WARNING: Display specs for " + specs.name + " are already registered! Replacing them with new ones...")
                MODEL.displaySpecs.update(specs.name, specs)
            } else {
                MODEL.displaySpecs.add(specs.name, specs)
            }
            socket.name = specs.name
            //Send the new display specs to all clients.
            broadcastEvent(null, 'incomingDisplaySpecs', specs)
            //Send the current model to this display.
            sendCurrentModelToDisplay()
        })

        //Add a 'close' event handler for the display socket.
        socket.on('close', function() {
            console.log('Display disconnected.')
            if (MODEL.displaySpecs.hasKey(socket.name)) {
                MODEL.displaySpecs.remove(socket.name)
                broadcastEvent(null, 'removeDisplaySpecs', socket.name)
            }
            LOCAL_DISPLAY = null
        })
    } else {
        console.log("---ERROR: Attempted to connect a second display to same server! Ignored.")
        socket.end()
    }
}).listen(3001)

//***********************************************************
//SOCKET.IO SERVER SETUP FOR CLIENT/CHILDSERVER COMMUNICATION
//***********************************************************
var io = socketio.listen(server, {log: false})
io.on('connection', function(socket) {
    socket.join('clients')
    NUM_CONNECTIONS++
    console.log("Client connected. socketId: " + socket.id + ", numConnected = " + NUM_CONNECTIONS)
    socket.on('error', console.error)

    socket.on('incomingImageNotify', function(data, ack) { handleImageNotify(socket, data, ack) })
    socket.on('incomingImage', function(data, ack) { handleIncomingImage(socket, data, ack) })
    socket.on('incomingAnnotation', function(data, ack) { handleIncomingAnnotation(socket, data, ack) })
    socket.on('updateImageFile', function(data, ack) { handleIncomingImageFile(socket, data, ack) })
    socket.on('updateImageLocation', function(data, ack) { handleUpdateImageLocation(socket, data, ack) })
    socket.on('resizeImage', function(data, ack) { handleResizeImage(socket, data, ack) })
    socket.on('bringImageToFront', function(data, ack) { handleBringImageToFront(socket, data, ack) })
    socket.on('sendImageToBack', function(data, ack) { handleSendImageToBack(socket, data, ack) })
    socket.on('requestFullImage', function(data, ack) { handleRequestFullImage(socket, data, ack) })
    socket.on('removeImage', function(data, ack) { handleRemoveImage(socket, data, ack) })
    socket.on('unlockImage', function(data, ack) { handleUnlockImage(socket, data, ack) })
    socket.on('clearWall', function(ack) { handleClearWall(socket, ack) })
    socket.on('requestModel', function(ack) {
        if(ack) ack()
        debugMsg('requestModel')
        sendCurrentModelToClient(socket)
    })
    socket.on('requestDisplaySpecs', function(ack) {
        if(ack) ack()
        debugMsg('requestDisplaySpecs')
        sendCurrentDisplaySpecsToClient(socket)
    })
    socket.on('incomingDisplaySpecs', function(data, ack){ handleIncomingDisplaySpecs(socket, data, ack) })
    socket.on('removeDisplaySpecs', function(data, ack){ handleRemoveDisplaySpecs(socket, data, ack) })
    socket.on('iAmServer', function(ack) {
        if(ack) ack()
        debugMsg('iAmServer')
        console.log("Connected client upgraded to server status. socketId: " + socket.id)
        // Subscribe this socket to the 'servers' room.
        socket.leave('clients')
        socket.join('servers')
    })
    socket.on('disconnect', function(reason){
        NUM_CONNECTIONS--
        console.log("Client disconnected. socketId: " + socket.id + ", reason: " + reason + ", numConnected = " + NUM_CONNECTIONS)
    })
})

//***********************************************************
//SOCKET.IO SERVER SETUP FOR COMMUNICATION WITH PARENT SERVER
//***********************************************************
if(remoteServer) {
    console.log("Connecting to parent server: " + remoteServer + ":" + port)
    // To enforce a strict tree structure of server and client connections, we disallow autoreconnection.
    // This avoids the possibility of circular connections arising.
    PARENT_SERVER = require('socket.io/node_modules/socket.io-client')('http://' + remoteServer + ':' + port, {reconnection: false})
    PARENT_SERVER.on('connect', function(){
        PARENT_SERVER.emit('iAmServer')
        PARENT_SERVER.emit('requestModel')
        PARENT_SERVER.emit('requestDisplaySpecs')
        console.log("Connected to parent server. Requesting model..")
    });
    PARENT_SERVER.on('connect_error', function(reason){
        console.log("Failed to connect to parent server, reason: " + reason)
    })
    PARENT_SERVER.on('connect_timeout', function(reason){
        console.log("Connection attempt with parent server timed out, reason: " + reason)
    })
    PARENT_SERVER.on('incomingImageNotify', function(data, ack) { handleImageNotify(PARENT_SERVER, data, ack) })
    PARENT_SERVER.on('incomingImage', function(data, ack) { handleIncomingImage(PARENT_SERVER, data, ack) })
    PARENT_SERVER.on('incomingAnnotation', function(data, ack) { handleIncomingAnnotation(PARENT_SERVER, data, ack) })
    PARENT_SERVER.on('updateImageFile', function(data, ack) { handleIncomingImageFile(PARENT_SERVER, data, ack) })
    PARENT_SERVER.on('updateImageLocation', function(data, ack) { handleUpdateImageLocation(PARENT_SERVER, data, ack) })
    PARENT_SERVER.on('resizeImage', function(data, ack) { handleResizeImage(PARENT_SERVER, data, ack) })
    PARENT_SERVER.on('bringImageToFront', function(data, ack) { handleBringImageToFront(PARENT_SERVER, data, ack) })
    PARENT_SERVER.on('sendImageToBack', function(data, ack) { handleSendImageToBack(PARENT_SERVER, data, ack) })
    PARENT_SERVER.on('removeImage', function(data, ack) { handleRemoveImage(PARENT_SERVER, data, ack) })
    PARENT_SERVER.on('unlockImage', function(data, ack) { handleUnlockImage(PARENT_SERVER, data, ack) })
    PARENT_SERVER.on('clearWall', function(ack) { handleClearWall(PARENT_SERVER, ack) })
    PARENT_SERVER.on('incomingDisplaySpecs', function(data, ack){ handleIncomingDisplaySpecs(PARENT_SERVER, data, ack) })
    PARENT_SERVER.on('removeDisplaySpecs', function(data, ack){ handleRemoveDisplaySpecs(PARENT_SERVER, data) })
    PARENT_SERVER.on('disconnect', function(reason){
        console.log("Disconnected from parent server, reason: " + reason)
    })
}

//***********************************************************
//******************** COMMON FUNCTIONS *********************
// These are functions shared by the the above server and
//  client sockets.
//***********************************************************

// This is just a short message, so that there can be 'instant' feedback on the display(s).
function handleImageNotify(socket, data, ack) {
    if(ack) ack()
    debugMsg('incomingImageNotify' + data.filename)
    if(!validImage(socket, data)){
        return
    }
    if (!MODEL.images.hasKey(data.filename)) {
        // Send image details to display for...  ..displaying.
        sendMessageToDisplay("$incoming-image$" + data.filename
                                + "$" + data.m_width
                                + "$" + data.m_height
                                + "$" + data.m_scale
                                + "$" + data.m_normalisedLocation.X
                                + "$" + data.m_normalisedLocation.Y
                                )
        // Send the notification to the other connected servers.
        broadcastEventToServers(socket, 'incomingImageNotify', data)
        // Save the thumbnail to the shared directory.
        if (data.m_thumbnail64) {
            fs.writeFile(fileSavePath + "/" + data.filename, new Buffer(data.m_thumbnail64, "base64"), function(err) {})
        } else {
            console.log("---WARNING: No thumbnail received!")
            sendLogMessageToClient(socket, "WARNING", "No thumbnail received on incomingImageNotify!")
        }
        // Record the brief upload on the server.
        var image = {incomplete: true, packetdata: data}
        MODEL.images.append(data.filename, image)
        //TODO: Add a timeout, after which the notification is cancelled if the upload still hasn't arrived.
    } else {
        console.log("---ERROR: Duplicate filename received! Ignored")
        sendLogMessageToClient(socket, "ERROR", "Duplicate incomingImageNotify received! Newer ignored.")
    }
}

function handleIncomingImage(socket, data, ack) {
    if(ack) ack()
    debugMsg('incomingImage' + data.filename)
    if(!validImage(socket, data)){
        return
    }
    //If no notification came in for this image earlier, quickly send one off to the display now.
    if (!MODEL.images.hasKey(data.filename)) {
        // Send image details to display for...  ..displaying.
        sendMessageToDisplay("$incoming-image$" + data.filename
                                + "$" + data.m_width
                                + "$" + data.m_height
                                + "$" + data.m_scale
                                + "$" + data.m_normalisedLocation.X
                                + "$" + data.m_normalisedLocation.Y
                                )
        var image = {incomplete: true, packetdata: data}
        MODEL.images.append(data.filename, image)
        sendLogMessageToClient(socket, "INFO", "incomingImage not preceded by an incomingImageNotify. Spoofing one.")
    }
    image = MODEL.images.get(data.filename)
    if (image.incomplete) {
        // Send the notification to the other connected servers.
        broadcastEventToServers(socket, 'incomingImage', data)
        // Save the image to the shared directory.
        if (data.m_image64) {
            fs.writeFile(fileSavePath + "/" + data.filename, new Buffer(data.m_image64, "base64"), function(err) {})
        } else {
            console.log("---ERROR: No image received!")
            sendLogMessageToClient(socket, "ERROR", "No image received on incomingImage!")
        }
        // Send the image object back to the clients without the image itself (just a thumbnail).
        data.m_image64 = ""
        // Broadcast the command to remaining clients/servers.
        broadcastEventToClients(socket, 'incomingShadowImage', data)
        // Record the full upload on the server.
        image.incomplete = false
        image.packetdata = data
        // Set up annotations
        image.annotations = new doublelist()
    } else {
        console.log("---ERROR: Received image object twice! Duplicate ignored.")
        sendLogMessageToClient(socket, "ERROR", "Duplicate incomingImage received! Newer ignored.")
    }
}

function handleIncomingAnnotation(socket, data, ack) {
    if(ack) ack()
    filenameSplit = data.imageFilename.match(/(.*)\.([^.]+)$/)
    data.filename = filenameSplit[1] + "_annot_" + data.m_ownerID + "." + filenameSplit[2]
    debugMsg('incomingAnnotation' + data.filename)
    if(!validFilename(socket, data.imageFilename)){
        return
    }
    if (MODEL.images.hasKey(data.imageFilename)){
        image = MODEL.images.get(data.imageFilename)
        if (!image.incomplete){
            // Broadcast the command to remaining clients/servers.
            broadcastEvent(socket, 'incomingAnnotation', data)

            // Record the update on the server. Annotation is not expected to be huge, so can store it in RAM.
            if(!image.annotations.hasKey(data.m_ownerID)){
                image.annotations.append(data.m_ownerID, "")
                // Send annotation specs to the display, so that it can handle the image properly when it arrives.
                sendMessageToDisplay("$incoming-annotation$" + data.imageFilename
                                    + "$" + data.filename
                                    + "$" + data.m_ownerID
                                    )
            }
            image.annotations.update(data.m_ownerID, data)
            // Write the new annotation to a file, for the display to render.
            if (data.m_layer64) {
                fs.writeFile(fileSavePath + "/" + data.filename, new Buffer(data.m_layer64, "base64"), function(err) {})
            } else {
                console.log("---ERROR: No annotation image received!")
                sendLogMessageToClient(socket, "ERROR", "No image received on incomingAnnotation!")
            }
        } else {
            console.log("---ERROR: Received annotation before receiving full image!")
            sendLogMessageToClient(socket, "ERROR", "Received incomingAnnotation before receiving full image.")
        }
    } else {
        console.log("---ERROR: Received annotation for nonexistent image!")
        sendLogMessageToClient(socket, "ERROR", "Received incomingAnnotation for nonexistent image.")
    }
}

function handleUpdateImageFile(socket, data, ack) {
    if(ack) ack()
    debugMsg('updateImageFile' + data.filename)
    if(!validFilename(socket, data.filename)){
        return
    }
    if (MODEL.images.hasKey(data.filename)){
        image = MODEL.images.get(data.filename)
        if (!image.incomplete){
            // Broadcast the command to remaining clients/servers.
            broadcastEvent(socket, 'updateImageFile', data)

            // Save the image to the shared directory.
            // NOTE: The display will automatically update itself when file in inbox changes.
            if (data.m_image64) {
                fs.writeFile(fileSavePath + "/" + data.filename, new Buffer(data.m_image64, "base64"), function(err) {})
            } else {
                console.log("---ERROR: No image received!")
                sendLogMessageToClient(socket, "ERROR", "No image received on updateImageFile!")
            }
        } else {
            console.log("---ERROR: Attempted to update image file before receiving full image!")
            sendLogMessageToClient(socket, "ERROR", "Received updateImageFile before receiving full image.")
        }
    } else {
        console.log("---ERROR: Attempted to move a update image file before receiving full image!")
        sendLogMessageToClient(socket, "ERROR", "Received updateImageFile for nonexistent image.")
    }
}

function handleUpdateImageLocation(socket, data, ack) {
    if(ack) ack()
    debugMsg('updateImageLocation' + data.filename)
    if(!validFilename(socket, data.filename)){
        return
    }
    if (MODEL.images.hasKey(data.filename)){
        image = MODEL.images.get(data.filename)
        if (!image.incomplete){
            // Send image details to display for...  ..displaying.
            sendMessageToDisplay("$update-image-location$" + data.filename
                                        + "$" + data.m_normalisedLocation.X
                                        + "$" + data.m_normalisedLocation.Y
                                        )
            // Broadcast the command to remaining clients/servers.
            broadcastEvent(socket, 'updateImageLocation', data)

            // Record the update on the server.
            image.packetdata.m_normalisedLocation.X = data.m_normalisedLocation.X
            image.packetdata.m_normalisedLocation.Y = data.m_normalisedLocation.Y

            // Prevent other clients from manipulating the image for now.
            lockImage(socket, image.packetdata)
        } else {
            console.log("---ERROR: Attempted to move image before receiving full image!")
            sendLogMessageToClient(socket, "ERROR", "Received updateImageLocation before receiving full image.")
        }
    } else {
        console.log("---ERROR: Attempted to move a nonexistent image!")
        sendLogMessageToClient(socket, "ERROR", "Received updateImageLocation for nonexistent image.")
    }
}

function handleResizeImage(socket, data, ack) {
    if(ack) ack()
    debugMsg('resizeImage' + data.filename)
    if(!validFilename(socket, data.filename)){
        return
    }
    if(!validScale(socket, data.m_scale)){
        return
    }
    if (MODEL.images.hasKey(data.filename)){
        image = MODEL.images.get(data.filename)
        if (!image.incomplete){
            //Send image details to display for...  ..displaying.
            sendMessageToDisplay("$resize-image$" + data.filename
                                        + "$" + data.m_scale
                                        )
            // Broadcast the command to remaining clients/servers.
            broadcastEvent(socket, 'resizeImage', data)

            //Record the update on the server.
            image.packetdata.m_scale = data.m_scale

            // Prevent other clients from manipulating the image for now.
            lockImage(socket, image.packetdata)
        } else {
            console.log("---ERROR: Attempted to resize an image before receiving full image!")
            sendLogMessageToClient(socket, "ERROR", "Received resizeImage before receiving full image.")
        }
    } else {
        console.log("---ERROR: Attempted to resize a nonexistent image!")
        sendLogMessageToClient(socket, "ERROR", "Received resizeImage for nonexistent image.")
    }
}

function handleBringImageToFront(socket, data, ack) {
    if(ack) ack()
    debugMsg('bringImageToFront' + data.filename)
    if(!validFilename(socket, data.filename)){
        return
    }
    if (MODEL.images.hasKey(data.filename)){
        image = MODEL.images.get(data.filename)
        if (!image.incomplete){
            //Send image details to display for...  ..displaying.
            sendMessageToDisplay("$bring-image-to-front$" + data.filename
                                        )
            // Broadcast the command to remaining clients/servers.
            broadcastEvent(socket, 'bringImageToFront', data)

            //Record the update on the server.
            MODEL.images.append(data.filename, MODEL.images.remove(data.filename))
        } else {
            console.log("---ERROR: Attempted to bring image to front before receiving full image!")
            sendLogMessageToClient(socket, "ERROR", "Received bringImageToFront before receiving full image.")
        }
    } else {
        console.log("---ERROR: Attempted to bring-to-front a nonexistent image!")
        sendLogMessageToClient(socket, "ERROR", "Received bringImageToFront for nonexistent image.")
    }
}

function handleSendImageToBack(socket, data, ack) {
    if(ack) ack()
    debugMsg('sendImageToBack' + data.filename)
    if(!validFilename(socket, data.filename)){
        return
    }
    if (MODEL.images.hasKey(data.filename)){
        image = MODEL.images.get(data.filename)
        if (!image.incomplete){
            //Send image details to display for...  ..displaying.
            sendMessageToDisplay("$send-image-to-back$" + data.filename
                                        )
            // Broadcast the command to remaining clients/servers.
            broadcastEvent(socket, 'sendImageToBack', data)

            //Record the update on the server.
            MODEL.images.push(data.filename, MODEL.images.remove(data.filename))
        } else {
            console.log("---ERROR: Attempted to send image to back before receiving full image!")
            sendLogMessageToClient(socket, "ERROR", "Received sendImageToBack before receiving full image.")
        }
    } else {
        console.log("---ERROR: Attempted to send-to-back a nonexistent image!")
        sendLogMessageToClient(socket, "ERROR", "Received sendImageToBack for nonexistent image.")
    }
}

function handleRequestFullImage(socket, data, ack) {
    if(ack) ack()
    debugMsg('requestFullImage' + data.filename)
    if(!validFilename(socket, data.filename)){
        return
    }
    fs.readFile(fileSavePath + "/" + data.filename, function(err, result) {
        data.m_image64 = result.toString('base64')
        socket.emit('fullImageSent', data)
    })
}

function handleRemoveImage(socket, data, ack) {
    if(ack) ack()
    debugMsg('removeImage' + data.filename)
    if(!validFilename(socket, data.filename)){
        return
    }
    if (MODEL.images.hasKey(data.filename)){
        image = MODEL.images.get(data.filename)
        if (!image.incomplete){
            //Send image details to display for...  ..displaying.
            sendMessageToDisplay("$remove-image$" + data.filename
                                        )
            // Broadcast the command to remaining clients/servers.
            broadcastEvent(socket, 'removeImage', data)

            //Record the update on the server.
            MODEL.images.remove(data.filename)
        } else {
            console.log("---ERROR: Attempted to remove image before receiving full image!")
            sendLogMessageToClient(socket, "ERROR", "Received removeImage before receiving full image.")
        }
    } else {
        console.log("---ERROR: Attempted to remove a nonexistent image!")
        sendLogMessageToClient(socket, "ERROR", "Received removeImage for nonexistent image.")
    }
}

function lockImage(socket, image){
    //console.log("LOCKING IMAGE: " + image.filename)
    image.timeout = 4
    if(!image.isLocked){
        image.isLocked = true
        broadcastEvent(socket, 'lockImage', {filename: image.filename})
        var timer = setInterval(function() {
            if(image.timeout-- <= 0){
                image.isLocked = false
                broadcastEvent(socket, 'unlockImage', {filename: image.filename})
                clearInterval(timer)
            }
        }, 500)
    }
}

function handleUnlockImage(socket, data, ack) {
    if(ack) ack()
    debugMsg('unlockImage' + data.filename)
    if (MODEL.images.hasKey(data.filename)){
        image = MODEL.images.get(data.filename)
        if (!image.incomplete){
            // Broadcast the command to remaining clients/servers.
            broadcastEvent(socket, 'unlockImage', data)

            //Record the update on the server.
            image.packetdata.isLocked = false
        } else {
            console.log("---ERROR: Attempted to unlock image before receiving full image!")
        }
    } else {
        console.log("---ERROR: Attempted to unlock a nonexistent image!")
    }
}

function handleClearWall(socket, ack) {
    if(ack) ack()
    debugMsg('clearWall')
    // Broadcast the command to remaining clients/servers.
    broadcastEmptyEvent(socket, 'clearWall')

    // Remove images from server
    MODEL.images.clear()

    // May as well clean all images out of the inbox while we're at it.
    fs.readdir(fileSavePath + "/", function(error, files) {
        if (error) throw error
        files.filter(function(filename){
            return /[a-zA-Z0-9_]*\.png$/.test(filename)
        }).forEach(function(filepath) {
            fs.unlink(fileSavePath + "/" + filepath, function(err){})
        })
    })
    sendMessageToDisplay("$clear-wall")
}

function handleIncomingDisplaySpecs(socket, data, ack) {
    if(ack) ack()
    debugMsg('displaySpecs')
    if (MODEL.displaySpecs.hasKey(data.name)) {
        console.log("---WARNING: Display specs for " + data.name + " are already registered! Replacing them with new ones...")
        MODEL.displaySpecs.update(data.name, data)
    } else {
        MODEL.displaySpecs.add(data.name, data)
    }
    broadcastEvent(socket, 'incomingDisplaySpecs', data)
}
function handleRemoveDisplaySpecs(socket, data, ack) {
    if(ack) ack()
    debugMsg('displaySpecs')
    MODEL.displaySpecs.remove(data)
    broadcastEvent(socket, 'removeDisplaySpecs', data)
}

//***********************************************************
//******************* VALIDATION FUNCTIONS ******************
//***********************************************************

function validImage(socket, data){
    return validFilename(socket, data.filename) && validScale(socket, data.m_scale)
}

function validFilename(socket, filename){
    if(filename.indexOf("$") > -1) {
        console.log("---ERROR: Received filename containing a dollar sign! (Received: " + filename + ")")
        sendLogMessageToClient(socket, "ERROR", "Image filename must not contain dollar signs. (Received: " + filename + ")")
        return false
    }
    return true
}

function validScale(socket, scale){
    if(scale <= 0) {
        console.log("---ERROR: Attempted to assign a non-positive scale factor to image! (Received: " + scale + ")")
        sendLogMessageToClient(socket, "ERROR", "ResizeImage value must be a positive scalar, representing the portion of screen height to be covered. (Received: " + scale + ")")
        return false
    }
    return true
}



//***********************************************************
//********************** AUX FUNCTIONS **********************
//***********************************************************

// A simple protocol that appends 4 bytes to the start of the message, indicating its length. (NET.SOCKET)
function sendMessageToDisplay(message){
    if(LOCAL_DISPLAY === null) return // Display not connected.
    var hex = message.length.toString(16).toUpperCase()
    LOCAL_DISPLAY.write(("0000" + hex).slice(-4) + message)
}

function sendLogMessageToClient(socket, priority, message){
    if(io.sockets.adapter.rooms['clients'] && io.sockets.adapter.rooms['clients'][socket.id]) {
        socket.emit('message', priority + ": " + message)
    }
}

// Sends out the packet with name <eventName> and payload <data> to all connected client/server
//  sockets.
// If <sender> is not null, then the packet will not be sent to that socket.
function broadcastEvent(sender, eventname, data){
    if(sender === null) {
        io.emit(eventname, data)
        if(remoteServer) { PARENT_SERVER.emit(eventname, data) }
    } else if(sender === PARENT_SERVER) {
        io.emit(eventname, data)
    } else {
        sender.broadcast.emit(eventname, data)
        if(remoteServer) { PARENT_SERVER.emit(eventname, data) }
    }
}
function broadcastEmptyEvent(sender, eventname){
    if(sender === null) {
        io.emit(eventname)
        if(remoteServer) { PARENT_SERVER.emit(eventname) }
    } else if(sender === PARENT_SERVER) {
        io.emit(eventname)
    } else {
        sender.broadcast.emit(eventname)
        if(remoteServer) { PARENT_SERVER.emit(eventname) }
    }
}
function broadcastEventToServers(sender, eventname, data){
    if(sender === null) {
        io.sockets.in('servers').emit(eventname, data)
        if(remoteServer) { PARENT_SERVER.emit(eventname, data) }
    } else if(sender === PARENT_SERVER) {
        io.sockets.in('servers').emit(eventname, data)
    } else {
        sender.broadcast.to('servers').emit(eventname, data)
        if(remoteServer) { PARENT_SERVER.emit(eventname, data) }
    }
}
function broadcastEmptyEventToServers(sender, eventname){
    if(sender === null) {
        io.sockets.in('servers').emit(eventname)
        if(remoteServer) { PARENT_SERVER.emit(eventname) }
    } else if(sender === PARENT_SERVER) {
        io.sockets.in('servers').emit(eventname)
    } else {
        sender.broadcast.to('servers').emit(eventname)
        if(remoteServer) { PARENT_SERVER.emit(eventname) }
    }
}
function broadcastEventToClients(sender, eventname, data){
    if(sender === null || sender === PARENT_SERVER) {
        io.sockets.in('clients').emit(eventname, data)
    } else {
        sender.broadcast.to('clients').emit(eventname, data)
    }
}
function broadcastEmptyEventToClients(sender, eventname){
    if(sender === null || sender === PARENT_SERVER) {
        io.sockets.in('clients').emit(eventname)
    } else {
        sender.broadcast.to('clients').emit(eventname)
    }
}
function broadcastLogMessageToClients(priority, message){
    io.sockets.in('clients').emit('message', priority + ": " + message)
}


// Sends out the list of currently connected displays to the selected client <client>.
// Note that the <client> argument is a socket.
function sendCurrentDisplaySpecsToClient(client){
    MODEL.displaySpecs.foreach(function(name, specs) {
        client.emit('incomingDisplaySpecs', specs)
    })
}

// Sends out the list of images currently on display.
// NOTE: This includes dimension specifications and a thumbnail, but not the actual image itself,
//  unless the target client socket is another server.
function sendCurrentModelToClient(client){
    if(client.rooms.indexOf('servers') > -1){
        MODEL.images.foreach(function(filename, image) {
            if(!image.incomplete) {
                // Rebuild the base64 encoding of the image (we deleted it earlier to conserve RAM)
                // NOTE: The following is an asynchronous call!
                fs.readFile(fileSavePath + "/" + filename, function(err, result) {
                    data = image.packetdata
                    data.m_image64 = result.toString('base64')
                    client.emit('incomingImage', data)
                    debugMsg("sent image to server: " + filename)
                    data.m_image64 = "" // remove the base64 encoding again afterwards.
                    image.annotations.foreach(function(ownerID, annotation_data) {
                        client.emit('incomingAnnotation', annotation_data)
                        debugMsg("sent annotation to client: " + annotation_data.filename)
                    })
                })
            }
        })
    } else {
        MODEL.images.foreach(function(filename, image) {
            if(!image.incomplete) {
                data = image.packetdata
                client.emit('incomingShadowImage', data)
                debugMsg("sent image to client: " + filename)
                image.annotations.foreach(function(ownerID, annotation_data) {
                    client.emit('incomingAnnotation', annotation_data)
                    debugMsg("sent annotation to client: " + filename)
                })
            }
        })
        client.emit('modelSent')
    }
}

// Sends out the list of images currently on display, to the display.
function sendCurrentModelToDisplay(){
    MODEL.images.foreach(function(filename, image) {
        data = image.packetdata
        // Incomplete images can be sent too, no wozza.
        sendMessageToDisplay("$incoming-image$" + filename
                             + "$" + data.m_width
                             + "$" + data.m_height
                             + "$" + data.m_scale
                             + "$" + data.m_normalisedLocation.X
                             + "$" + data.m_normalisedLocation.Y
                            )
        debugMsg("sent image to display: " + filename)
        image.annotations.foreach(function(ownerID, annotation_data) {
            sendMessageToDisplay("$incoming-annotation$" + filename
                                 + "$" + annotation_data.filename
                                 + "$" + ownerID
                                 )
            debugMsg("sent annotation to display: " + annotation_data.filename)
        })
    })
}