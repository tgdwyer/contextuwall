#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <omega.h>
#include <omegaToolkit.h>
#include <omegaGl.h>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/ptree.hpp>
//#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/json_parser.hpp>

#define INITIAL_X_LOCATION 400

#define EVENT_SIZE (sizeof (struct inotify_event))
#define EVENT_BUF_LEN (1024 * (EVENT_SIZE + 16))

//#define CONTEXUWALL_INBOX_PATH "../../ContextuWallServer/inbox"
//#define WEB_SERVER_IP "130.194.70.46"
//#define WEB_SERVER_IP "head1.cave.monash.edu"
//#define WEB_SERVER_IP "10.0.0.10"
//#define HEAD_NODE_IP "opti-merc1"
//#define HEAD_NODE_IP "spelunk"
//#define WEB_SERVER_IP "opti-merc1.monash.edu"
//#define COMMAND_PORT 3001

using namespace omega;
using namespace omegaToolkit;
using namespace omegaToolkit::ui;
using boost::property_tree::ptree;

class ImageContainer;
class AnnotationContainer;
class InboxWatcher;
class CommandListener;

class ContextuWallApplication: public EngineModule {
public:
  ContextuWallApplication();
  ~ContextuWallApplication();
  virtual void initialize();

  virtual void commitSharedData(SharedOStream& out);
  virtual void updateSharedData(SharedIStream& in);

  virtual void update(const UpdateContext& context);
  virtual void handleEvent(const Event& evt) {}
  virtual bool handleCommand(const String& cmd) { return false; }

  string m_nodeLogPrefix;
  bool m_isHeadNode;
  string m_headNodeIP;
  string m_hostname;
  string m_headHostname;
  string m_commandServerIP;
  int m_commandServerPort;
  string m_contextuWallInboxPath;
  int m_serverSocketId;

  int m_screenWidth;
  int m_screenHeight;

  List<string> m_uncommittedCommands;
  List<string> m_committedCommands;

  ptree m_contextuWallParameters;

  // The ui manager
  Ref<UiModule> m_uiModule;
  // The root ui container
  Ref<Container> m_ui;

  // Widgets
  std::map <string, ImageContainer*> myImageContainerMap;
  std::map <string, AnnotationContainer*> myAnnotationContainerMap;

  InboxWatcher *myInboxWatcher;
  CommandListener *myCommandListener;
  Lock *m_commandLock;

  virtual void indexImage(string, Vector2f, float, Vector2f);
  virtual void indexAnnotation(string, string, string);
  virtual void showSharedData();
  virtual void clearWall();
  virtual bool doesFileExist(string);
private:
  // Widgets
  Ref<Label> myMainLabel;
  Ref<Label> myStatusLabel;
};

class ImageContainer {
public:
  void initialize(ContextuWallApplication* app, string filename, Vector2f imageSize, float scale, Vector2f location);
  void animateEntrance();
  void loadImage();
  void imageLoaded(Ref<PixelData> imgData);
  void setLocation(Vector2f location);
  void setScale(float scale);
  void highlight(bool on);
  void bringToFront();
  void sendToBack();
  void remove();
  ContextuWallApplication* app;
  Image* m_image;
  List< Ref<ImageUtils::LoadImageAsyncTask> > m_imageLoader;
  string m_filename;
  string m_fullPath;
  Vector2f m_imageSize;
  Vector2f m_location;
  Vector2f m_animLocation;
  float m_scale;
  float m_animScale;
  int m_highlightTimeout;
  bool m_isContentLoaded;
  int m_animationStage;
  bool m_isQueuedForRemoval;

  List< AnnotationContainer* > m_annotations;

  Ref<Container> m_container;
  Ref<Container> m_imagePlaceholder;
};

class AnnotationContainer {
public:
  void initialize(ImageContainer* parentImage, string filename, string ownerID);
  void loadImage();
  void imageLoaded(Ref<PixelData> imgData);
  void bringToFront();
  void sendToBack();
  void remove();
  ImageContainer* parentImage;
  Image* m_image;
  List< Ref<ImageUtils::LoadImageAsyncTask> > m_imageLoader;
  string m_filename;
  string m_fullPath;
  string m_ownerID;
};

class InboxWatcher: public Thread {
public:
  InboxWatcher(ContextuWallApplication*);
  ~InboxWatcher();
  virtual void addPath(string);
  virtual void threadProc();
private:
  int fd;
  int wd;
  char buffer[EVENT_BUF_LEN];
  ContextuWallApplication *app;
};

class CommandListener: public Thread {
public:
  CommandListener(ContextuWallApplication*);
  ~CommandListener();
  virtual void threadProc();
private:
  int sockfd, portno, n;
  struct sockaddr_in serv_addr;
  struct hostent *server;
  int connectionResult;
  ContextuWallApplication *app;
};
