#include "ContextuWall.h"

using namespace omega;
using namespace omegaToolkit;
using namespace omegaToolkit::ui;
using boost::property_tree::ptree;

void log(boost::format msg) {
  omsg(msg.str());
}

const Vector2f PADDING_OFFSET = Vector2f(10,10);

///////////////////////////////////////////////////////////////////////////////
void ImageContainer::initialize(ContextuWallApplication* app, string filename, Vector2f imageSize, float scale, Vector2f location) {
    this->app = app;
    this->m_filename = filename;
    this->m_fullPath = app->m_contextuWallInboxPath + "/" + m_filename;
    this->m_imageSize = imageSize;
    this->m_scale = scale;

    m_container = Container::create(Container::LayoutFree, app->m_ui);
    m_container->setStyle("border: 1 #ffffff");
    highlight(false);

    m_imagePlaceholder = Container::create(Container::LayoutHorizontal, m_container);
    m_imagePlaceholder->setVerticalAlign(Container::AlignMiddle);
    m_imagePlaceholder->setHorizontalAlign(Container::AlignCenter);
    m_imagePlaceholder->setStyle("fill: #ffaa0040");
    // Since forced to use FreeLayout mode so that we can overlap images,
    //  we need to apply padding manually.
    m_imagePlaceholder->setPosition(PADDING_OFFSET);
    Label* fnt = Label::create(m_imagePlaceholder);
    fnt->setSize(m_imageSize * m_scale);
    fnt->setStyle("align: middle-center");
    fnt->setFont("fonts/arial.ttf 50");
    fnt->setColor(Color::White);
    fnt->setText("LOADING...");
    fnt->setAutosize(false);

    setLocation(location);

    m_isContentLoaded = false;
    m_isQueuedForRemoval = false;
    m_animationStage = -1;
    m_container->setVisible(false);
}

void* _animateEntrance(void* arg) {
    ImageContainer *myImageContainer = (ImageContainer*)arg;
    double revertRate = 0.999f;
    int left = myImageContainer->m_location[0];
    int originalTop = myImageContainer->m_location[1];
    int top = myImageContainer->app->m_screenHeight;
    float originalScale = myImageContainer->m_scale;
    float scale = originalScale * 0.2;
    myImageContainer->m_animationStage = 0;
    while (top != originalTop) {
        top = originalTop + (int)((top-originalTop) * revertRate);
        myImageContainer->m_animLocation = Vector2f(left, top);
        scale = scale + (originalScale-scale) * (1-revertRate);
        myImageContainer->m_animScale = scale;
        revertRate -= 0.001f;
        usleep(5000);
    }
    myImageContainer->m_animScale = originalScale;
    myImageContainer->m_animationStage = 1;
}

void ImageContainer::animateEntrance(){
    pthread_t t1;
    pthread_create(&t1, NULL, &_animateEntrance, (void*)this);
}

// NOTE: This current setup works efficiently, but is occasionally visually unimpressive,
//  since the child nodes aren't synced and will occasionally display their portion of the
//  image visibly earlier/later than the others.
void ImageContainer::loadImage() {
    m_imageLoader.push_back(ImageUtils::loadImageAsync(m_fullPath, true));
}

void ImageContainer::imageLoaded(Ref<PixelData> imgData) {
    // Push image to the container.
    if(!m_isContentLoaded){
        m_isContentLoaded = true;
        m_image = Image::create(m_container);
        // Since forced to use FreeLayout mode so that we can overlap images,
        //  we need to apply padding manually.
        m_image->setPosition(PADDING_OFFSET);
        m_imagePlaceholder->setVisible(false);
        m_container->removeChild(m_imagePlaceholder);
    }
    m_image->setData(imgData.get());
    setScale(m_scale);
    setLocation(m_location);
    // Send image to back so that it doesn't cover any existing annotations.
    m_image->setLayer(Widget::Back);
    highlight(true);
}

void ImageContainer::setLocation(Vector2f location) {
    this->m_location = location;
    m_container->setPosition(m_location - m_imageSize/2 * m_scale - PADDING_OFFSET);
}

void ImageContainer::setScale(float scale) {
    this->m_scale = scale;
    Vector2f newSize = m_imageSize * m_scale;
    if (m_isContentLoaded){
        m_image->setSize(newSize);
        foreach (AnnotationContainer *annotation, m_annotations) {
            annotation->m_image->setSize(newSize);
        }
    } else {
        m_imagePlaceholder->getChildByIndex(0)->setSize(newSize);
    }
    setLocation(m_location);
}

void ImageContainer::highlight(bool on) {
    if(on){
        m_container->setStyle("fill: #ffaa00ff");
        m_highlightTimeout = 20; //update loop ticks
    } else {
        m_container->setStyle("fill: #00000080");
    }
}

void ImageContainer::bringToFront() {
    app->m_ui->removeChild(m_container);
    app->m_ui->addChild(m_container);
}
void ImageContainer::sendToBack() {
    app->m_ui->removeChild(m_container);
    app->m_ui->addChild(m_container);

    //Iterate over all other children and re-add them.. (no other way.. ;/)
    Widget* w = app->m_ui->getChildByIndex(0);
    while (w != m_container) {
        app->m_ui->removeChild(w);
        app->m_ui->addChild(w);
        w = app->m_ui->getChildByIndex(0);
    }
}

void ImageContainer::remove() {
    m_container->setVisible(false);
    app->m_ui->removeChild(m_container);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AnnotationContainer::initialize(ImageContainer* parentImage, string filename, string ownerID) {
    this->parentImage = parentImage;
    this->m_filename = filename;
    this->m_fullPath = parentImage->app->m_contextuWallInboxPath + "/" + m_filename;
    this->m_ownerID = ownerID;

    this->m_image = Image::create(parentImage->m_container);
    m_image->setPosition(PADDING_OFFSET);
    parentImage->m_annotations.push_front(this);
}

void AnnotationContainer::loadImage() {
    m_imageLoader.push_back(ImageUtils::loadImageAsync(m_fullPath, true));
}

void AnnotationContainer::imageLoaded(Ref<PixelData> imgData) {
    // Push image to the container.
    m_image->setData(imgData.get());
    m_image->setSize(parentImage->m_imageSize * parentImage->m_scale);
    parentImage->highlight(true);
}

void AnnotationContainer::bringToFront() {
    //TODO: In the distant future
}
void AnnotationContainer::sendToBack() {
    //TODO: In the distant future
}

void AnnotationContainer::remove() {
    parentImage->m_annotations.remove(this);
    parentImage->m_container->removeChild(m_image);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

CommandListener::CommandListener(ContextuWallApplication *app) : Thread() {
    //omsg("\n---CommandListener()");
    this->app = app;
    portno = app->m_commandServerPort;
}
CommandListener::~CommandListener() {
    close(sockfd);
}

void* runCommand(void* arg) {
    char buffer[256];
    ContextuWallApplication *app = (ContextuWallApplication*)arg;

    // Send display specs before commencing listening cycle.
    sprintf(buffer,"$display-specs$%s$%i$%i", (app->m_hostname).c_str(), app->m_screenWidth, app->m_screenHeight);
    int n = write(app->m_serverSocketId, buffer, strlen(buffer));
    if (n < 0) omsg("\n---ERROR writing to socket");

    while (true) {
        bzero(buffer,256);
        char *p;
        char lenEncoded[5];
        int n = -1;
        while (n < 0){
            n = read(app->m_serverSocketId, &lenEncoded, 4);
        }
        if (n == 0) {
            omsg("\n---Server closed the socket");
            app->m_commandLock->lock();
            app->m_uncommittedCommands.push_back("$clear-wall");
            app->m_uncommittedCommands.push_back("$set-connection-status$Disconnected");
            app->m_commandLock->unlock();

            // Can't reuse socket after a disconnect, so destroy it and create a new one.
            close(app->m_serverSocketId);
            app->myCommandListener->start();
            return 0;
        }
        // Convert hex string to integer.
        lenEncoded[4] = '\0';
        long len = std::strtoul(lenEncoded, &p, 16);
        //printf("\n---runCommand: Incoming message of length: %i\n", len);
        n = read(app->m_serverSocketId, buffer, len);
        printf("\n---incoming Command: %s\n", buffer);
        string info(buffer);
        // Push the command to the queue
        app->m_commandLock->lock();
        app->m_uncommittedCommands.push_back(info);
        app->m_commandLock->unlock();
    }
}

void CommandListener::threadProc() {
    //omsg("\n---CommandListener::threadProc");

    // Create and set up socket.
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        omsg("\n---ERROR opening socket");
        app->m_commandLock->lock();
        app->m_uncommittedCommands.push_back("$set-connection-status$Error opening socket");
        app->m_commandLock->unlock();
    } else {
        server = gethostbyname(app->m_commandServerIP.c_str());
        if (server == NULL) {
            omsg("\n---ERROR, no such host\n");
        } else {
            bzero((char *)&serv_addr, sizeof(serv_addr));
            serv_addr.sin_family = AF_INET;
            bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
            serv_addr.sin_port = htons(portno);
            app->m_serverSocketId = sockfd;
        }
    }
    // Establish connection
    while (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
        omsg("\n---CommandListener::threadProc: ERROR connecting, trying again in 5s.");
        sleep(5);
    }
    omsg("\n---CommandListener::threadProc: Connected.");
    app->m_commandLock->lock();
    app->m_uncommittedCommands.push_back("$set-connection-status$Connected to server");
    app->m_commandLock->unlock();

    pthread_t t1;
    pthread_create(&t1, NULL, &runCommand, (void*)app);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

InboxWatcher::InboxWatcher(ContextuWallApplication *app): Thread() {
    this->app = app;
    fd = inotify_init();
};

InboxWatcher::~InboxWatcher() {
    inotify_rm_watch(fd, wd);
    close(fd);
};

void InboxWatcher::threadProc() {
    //omsg("\n---InboxWatcher::threadProc");
    char messageBuffer[256];
    std::map<string, ImageContainer*>::iterator it;
    std::map<string, AnnotationContainer*>::iterator ait;
    while(true) {
        //omsg("\n---checking path");
        struct pollfd pfd = {fd, POLLIN, 0};
        int ret = poll(&pfd, 1, 50);  // timeout of 50ms
        if (ret < 0) {
            //fprintf(stderr, "poll failed: %s\n", strerror(errno));
        } else if (ret == 0) {
            // Timeout with no events, move on.
            //omsg("timeout");
        } else {
            // Process the new event.
            int length = read(fd, buffer, EVENT_BUF_LEN);
            int i = 0;
            while (i < length) {
                struct inotify_event *event = (struct inotify_event *)&buffer[i];
                if (event->len) {
                    if (event->mask & IN_CREATE) {
                        if (!(event->mask & IN_ISDIR)) {
                            ofmsg("\n---InboxWatcher-IN_CREATE: %s", %event->name);
                        }
                    } else if (event->mask & IN_CLOSE_WRITE) {
                        log(boost::format("\n---InboxWatcher-IN_CLOSE_WRITE: %s") %event->name);
                        it = app->myImageContainerMap.find(event->name);
                        if (it != app->myImageContainerMap.end()){
                            sprintf(messageBuffer,"$load-image$%s", event->name);
                            app->m_commandLock->lock();
                            app->m_uncommittedCommands.push_back(messageBuffer);
                            app->m_commandLock->unlock();
                        } else {
                            ait = app->myAnnotationContainerMap.find(event->name);
                            if (ait != app->myAnnotationContainerMap.end()) {
                                sprintf(messageBuffer,"$load-annotation$%s", event->name);
                                app->m_commandLock->lock();
                                app->m_uncommittedCommands.push_back(messageBuffer);
                                app->m_commandLock->unlock();
                            }
                        }
                    }
                }
                i += EVENT_SIZE + event->len;
            }
        }
        sleep(1);
    };
};
void InboxWatcher::addPath(string path) {
  //ofmsg("\n---InboxWatcher::addPath path=%1%", %path);
  wd = inotify_add_watch(fd, path.c_str(), IN_CREATE | IN_DELETE | IN_CLOSE_WRITE);
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ContextuWallApplication::ContextuWallApplication(): EngineModule("ContextuWallApplication") {
    enableSharedData();
    m_isHeadNode = false;
};

//WARNING: Do NOT call this method from the child threads!
// Doing so can freeze up their file indexing.
bool ContextuWallApplication::doesFileExist(string filename) {
    return (access(filename.c_str(), F_OK) != -1);
}

void ContextuWallApplication::showSharedData() {
    //omsg("----------->ShowSharedData");
    vector<string> strs;
    std::map<string, ImageContainer*>::iterator it;
    std::map<string, AnnotationContainer*>::iterator ait;
    string command;

    while(!m_committedCommands.empty()){
        command = m_committedCommands.front();
        m_committedCommands.pop_front();
        omsg("\n---running Command: " + command);
        if (command.length() > 0){
            boost::split(strs, command, boost::is_any_of("$"));
            if (strs.size() >= 2){
                if (strs[1].compare("incoming-image") == 0) {//================================
                    it = myImageContainerMap.find(strs[2]);
                    if (it == myImageContainerMap.end()) {
                        string incomingImageFile = strs[2];
                        float width = (int)(boost::lexical_cast<double>(strs[3]));
                        float height = (int)(boost::lexical_cast<double>(strs[4]));
                        float scale = (float)(boost::lexical_cast<double>(strs[5]));
                        // Convert the scale to adjust from native size instead of wall height.
                        scale = m_screenHeight / height * scale;
                        int x = (int)(boost::lexical_cast<double>(strs[6]) * m_screenWidth);
                        int y = (int)(boost::lexical_cast<double>(strs[7]) * m_screenHeight);
                        indexImage(incomingImageFile, Vector2f(width, height), scale, Vector2f(x, y));
                    }
                } else if (strs[1].compare("incoming-annotation") == 0) {//================================
                    it = myImageContainerMap.find(strs[2]);
                    if (it != myImageContainerMap.end()) {
                        string imageFile = strs[2];
                        string incomingAnnotationFile = strs[3];
                        string annotationOwnerID = strs[4];
                        indexAnnotation(imageFile, incomingAnnotationFile, annotationOwnerID);
                    }
                } else if (strs[1].compare("load-image") == 0) {//================================
                    it = myImageContainerMap.find(strs[2]);
                    if (it != myImageContainerMap.end()) {
                        ImageContainer *myImageContainer = myImageContainerMap[strs[2]];
                        myImageContainer->loadImage();
                    }
                } else if (strs[1].compare("load-annotation") == 0) {//================================
                    ait = myAnnotationContainerMap.find(strs[2]);
                    if (ait != myAnnotationContainerMap.end()) {
                        AnnotationContainer *myAnnotationContainer = myAnnotationContainerMap[strs[2]];
                        myAnnotationContainer->loadImage();
                    }
                } else if (strs[1].compare("update-image-location") == 0) {//================================
                    it = myImageContainerMap.find(strs[2]);
                    if (it != myImageContainerMap.end()) {
                        ImageContainer *myImageContainer = myImageContainerMap[strs[2]];
                        int x = (int)(boost::lexical_cast<double>(strs[3]) * m_screenWidth);
                        int y = (int)(boost::lexical_cast<double>(strs[4]) * m_screenHeight);
                        myImageContainer->setLocation(Vector2f(x, y));
                        myImageContainer->highlight(true);
                    }
                } else if (strs[1].compare("resize-image") == 0) {//================================
                    it = myImageContainerMap.find(strs[2]);
                    if (it != myImageContainerMap.end()) {
                        ImageContainer *myImageContainer = myImageContainerMap[strs[2]];
                        float scale = (float)(boost::lexical_cast<double>(strs[3]));
                        // Convert the scale to adjust from native size instead of wall height.
                        scale = m_screenHeight / myImageContainer->m_imageSize[1] * scale;
                        myImageContainer->setScale(scale);
                        myImageContainer->highlight(true);
                    }
                } else if (strs[1].compare("bring-image-to-front") == 0) {//================================
                    it = myImageContainerMap.find(strs[2]);
                    if (it != myImageContainerMap.end()) {
                        ImageContainer *myImageContainer = myImageContainerMap[strs[2]];
                        myImageContainer->bringToFront();
                        myImageContainer->highlight(true);
                    }
                } else if (strs[1].compare("send-image-to-back") == 0) {//================================
                    it = myImageContainerMap.find(strs[2]);
                    if (it != myImageContainerMap.end()) {
                        ImageContainer *myImageContainer = myImageContainerMap[strs[2]];
                        myImageContainer->sendToBack();
                        myImageContainer->highlight(true);
                    }
                } else if (strs[1].compare("remove-image") == 0) {//================================
                    it = myImageContainerMap.find(strs[2]);
                    if (it != myImageContainerMap.end()) {
                        ImageContainer *myImageContainer = myImageContainerMap[strs[2]];
                        myImageContainer->m_isQueuedForRemoval = true;
                    }
                } else if (strs[1].compare("clear-wall") == 0) {//================================
                    clearWall();
                } else if (strs[1].compare("set-config") == 0) {//================================
                    m_headHostname = strs[2];
                    m_contextuWallInboxPath = strs[3];
                    m_screenWidth = (int)boost::lexical_cast<int>(strs[4]);
                    m_screenHeight = (int)boost::lexical_cast<int>(strs[5]);
                    myMainLabel->setText("ContextuWall: " + m_headHostname + " (" + strs[4] + "x" + strs[5] + ")");
                } else if (strs[1].compare("set-connection-status") == 0) {//================================
                    myStatusLabel->setText("Status: " + strs[2]);
                } else {
                    omsg("---ERROR: Unknown command: " + command);
                    // Unhandled command.
                }
            }
        }
    }
}

// This code runs on the head node. It is used to send data to the child nodes via a shared stream.
void ContextuWallApplication::commitSharedData(SharedOStream& out) {
    //omsg("----------->CommitSharedData");
    string commandQueue = "";

    // Serialise the command queue.
    m_commandLock->lock();
    while(!m_uncommittedCommands.empty()){
        string command = m_uncommittedCommands.front();
        commandQueue.append("!" + command);
        m_committedCommands.push_back(command);
        //omsg("Attaching: " + command);
        m_uncommittedCommands.pop_front();
    }
    m_commandLock->unlock();
    //omsg("Committing: " + commandQueue);

    // Push content to the stream.
    out << commandQueue;

    //omsg("\n---ContextuWallApplication::commitShareData");
    showSharedData();
}

// This code runs on the child nodes.
void ContextuWallApplication::updateSharedData(SharedIStream& in) {
    //omsg("----------->UpdateSharedData");
    vector<string> strs;
    string commandQueue = "";

    // Pull content from the stream.
    in >> commandQueue;

    //omsg("Updating: " + commandQueue);

    // Deserialise the command queue.
    boost::split(strs, commandQueue, boost::is_any_of("!"));
    foreach (string command, strs) {
        m_committedCommands.push_back(command);
        //omsg("Receiving: " + command);
    }

    //omsg("\n---ContextuWallApplication::updateShareData");
    showSharedData();
}

ContextuWallApplication::~ContextuWallApplication() {
  std::map<string, ImageContainer*>::iterator it;
  for (it = myImageContainerMap.begin(); it != myImageContainerMap.end(); ++it) {
    delete it->second;
  };
  myImageContainerMap.clear();
  std::map<string, AnnotationContainer*>::iterator ait;
  for (ait = myAnnotationContainerMap.begin(); ait != myAnnotationContainerMap.end(); ++ait) {
    delete ait->second;
  };
  myAnnotationContainerMap.clear();
  delete myInboxWatcher;
  delete myCommandListener;
};

void ContextuWallApplication::initialize() {
    char buffer[256];
    buffer[255] = '\0';
    gethostname(buffer, 255);
    std::string runningHostname(buffer);
    m_hostname = runningHostname;

    SystemManager* sys = SystemManager::instance();
    m_isHeadNode = sys->isMaster();
    m_commandLock = new Lock();

    // Prints additional information regarding loadImageAsync to the console.
    ImageUtils::setVerbose(true);

    if (m_isHeadNode){
        m_headHostname = m_hostname;
        std::stringstream ss;
        std::string jsonSettings = "ContextuWall.settings.json";
        std::ifstream in;
        // Check if there exists a config file with hostname appended at end.
        if (doesFileExist(jsonSettings + "." + m_headHostname)){
            omsg("\n---Using custom json settings file: " + jsonSettings + "." + m_headHostname);
            in.open((jsonSettings + "." + m_headHostname).c_str(), std::ios::binary);
        } else {
            omsg("\n---Using default json settings file: " + jsonSettings);
            in.open(jsonSettings.c_str(), std::ios::binary);
        }
        ss << in.rdbuf();
        // Read in the configuration file.
        boost::property_tree::read_json(ss, m_contextuWallParameters);
        m_commandServerIP = m_contextuWallParameters.get<std::string>("commandServerIP");
        m_commandServerPort = m_contextuWallParameters.get<int>("commandServerPort");
        m_contextuWallInboxPath = m_contextuWallParameters.get<std::string>("contextuWallInboxPath");

        // Regrettably, we cannot currently access the display dimensions, even though omegalib knows them.
        //  Hence we're forced to read them in from a config file and broadcast them to the child nodes...
        m_screenWidth = m_contextuWallParameters.get<int>("screenWidth");
        m_screenHeight = m_contextuWallParameters.get<int>("screenHeight");
        sprintf(buffer,"$set-config$%s$%s$%i$%i", m_headHostname.c_str(), m_contextuWallInboxPath.c_str(), m_screenWidth, m_screenHeight);
        m_commandLock->lock();
        m_uncommittedCommands.push_back(buffer);
        m_commandLock->unlock();
    }

    DisplaySystem* ds = sys->getDisplaySystem();
//     DisplayConfig& displayConfig = ds->getDisplayConfig();
//     MultiInstanceConfig multiInstanceConfig = sys->getMultiInstanceConfig();
//     for(int i = 0; i < displayConfig.numNodes; i++) {
//         //omsg((boost::format("\n---hostname=%s") %displayConfig.nodes[i].hostname).str());
//         DisplayTileConfig* dtc = displayConfig.nodes[i].tiles[0];
//         //omsg((boost::format("\n---isInGrid=%s") %dtc->isInGrid).str());
//     }
//     Dictionary<String, DisplayTileConfig *> tiles = displayConfig.tiles;
//     Dictionary<String, DisplayTileConfig *>::iterator it;
//     for (it = tiles.begin(); it != tiles.end(); it++) {
//         //omsg((boost::format("\n---it=%s") %it->first).str());
//     }
    //DisplayConfig& displayConfig = ds->getDisplayConfig();
    //printf("dims: %ix%i\n", (ds->getCanvasSize()[0], ds->getCanvasSize()[1]));
    //printf("dims2: %ix%i\n", (displayConfig.canvasPixelSize[0], displayConfig.canvasPixelSize[1]));
    ds->setBackgroundColor(Color(0.1f,0.1f,0.15f));

    // Create and initialize the UI management module.
    m_uiModule = UiModule::createAndInitialize();
    m_ui = m_uiModule->getUi();

    myMainLabel = Label::create(m_ui);
    myMainLabel->setFont("fonts/arial.ttf 30");
    myMainLabel->setText("ContextuWall");

    myStatusLabel = Label::create(m_ui);
    myStatusLabel->setFont("fonts/arial.ttf 30");
    myStatusLabel->setText("Status: ");
    myStatusLabel->setPosition(Vector2f(0, 30));

    if (m_isHeadNode) {
        m_nodeLogPrefix = "HEAD-NODE";
        ofmsg("\n---ContextuWall HeadNode started: %1%", %m_hostname);
        myInboxWatcher = new InboxWatcher(this);
        myCommandListener = new CommandListener(this);

        myInboxWatcher->start();
        myInboxWatcher->addPath(m_contextuWallInboxPath);

        myCommandListener->start();
    } else {
        m_nodeLogPrefix = "CHILD-NODE";
        ofmsg("\n---ContextuWall ChildNode started!: %1%", %m_hostname);
    }
}

void ContextuWallApplication::indexImage(string filename, Vector2f size, float scale, Vector2f location) {
    //log(boost::format("\n---ContextuWallApplication::indexImage: %s") %filename);

    ImageContainer *myImageContainer = new ImageContainer();
    // Create new container for the image.
    myImageContainer->initialize(this, filename, size, scale, location);
    myImageContainerMap[filename] = myImageContainer;

    // Animate entrance!
    myImageContainer->animateEntrance();

    // Perhaps the image is already available? Check!
    // NOTE: The file existence check MUST be performed on the head node, otherwise the file indexing
    //  gets messed up on the child nodes and they end up thinking there's no image even once one arrives.
    if (m_isHeadNode && doesFileExist(myImageContainer->m_fullPath)){
        //log(boost::format("\n---ContextuWallApplication::indexImage: File already exists, drawing immediately: %s") %filename);
        char messageBuffer[256];
        sprintf(messageBuffer,"$load-image$%s", filename.c_str());
        m_commandLock->lock();
        m_uncommittedCommands.push_back(messageBuffer);
        m_commandLock->unlock();
    }
};

void ContextuWallApplication::indexAnnotation(string imageFile, string filename, string ownerID){
    ImageContainer *myImageContainer = myImageContainerMap[imageFile];
    AnnotationContainer *myAnnotationContainer = new AnnotationContainer();
    // Create new container for the annotation
    myAnnotationContainer->initialize(myImageContainer, filename, ownerID);
    myAnnotationContainerMap[filename] = myAnnotationContainer;

    // Check if file already available.
    // NOTE: The file existence check MUST be performed on the head node, otherwise the file indexing
    //  gets messed up on the child nodes and they end up thinking there's no image even once one arrives.
    if (m_isHeadNode && doesFileExist(myAnnotationContainer->m_fullPath)){
        //log(boost::format("\n---ContextuWallApplication::indexImage: File already exists, drawing immediately: %s") %filename);
        char messageBuffer[256];
        sprintf(messageBuffer,"$load-annotation$%s", filename.c_str());
        m_commandLock->lock();
        m_uncommittedCommands.push_back(messageBuffer);
        m_commandLock->unlock();
    }
}

void ContextuWallApplication::clearWall() {
    log(boost::format("\n---ContextuWallApplication::clearWall"));
    std::map<string, ImageContainer*>::iterator it;
    for (it = myImageContainerMap.begin(); it != myImageContainerMap.end(); ++it) {
        ImageContainer *myImageContainer = (ImageContainer*)&(*it->second);
        myImageContainer->m_isQueuedForRemoval = true;
    };
}

void ContextuWallApplication::update(const UpdateContext& context) {
    //omsg("\n---ContextuWallApplication::update");
    std::map<string, ImageContainer*>::iterator it;
    for (it = myImageContainerMap.begin(); it != myImageContainerMap.end(); ++it) {
        ImageContainer *myImageContainer = (ImageContainer*)&(*it->second);
        if(myImageContainer->m_animationStage >= 0){
            myImageContainer->m_container->setVisible(true);
            myImageContainer->setLocation(myImageContainer->m_animLocation);
            myImageContainer->setScale(myImageContainer->m_animScale);
            if(myImageContainer->m_animationStage == 1){
                myImageContainer->m_animationStage = -1;
            }
        }
        if(myImageContainer->m_highlightTimeout-- == 0){
            myImageContainer->highlight(false);
        }
        if(myImageContainer->m_imageLoader.size() > 0){
            Ref<ImageUtils::LoadImageAsyncTask> task = myImageContainer->m_imageLoader.front();
            if(task->isComplete()){
                Ref<PixelData> pixels = task->getData().image;
                if(pixels == NULL){
                    omsg("======================NULL PIXELS!!=======================");
                    // Failed to load properly, attempt a reload.
                    //myImageContainer->loadImage();
                } else {
                    myImageContainer->imageLoaded(pixels);
                }
                myImageContainer->m_imageLoader.pop_front();
            }
        } else {
            if(myImageContainer->m_isQueuedForRemoval){
                myImageContainer->remove();
                delete it->second;
                myImageContainerMap.erase(it);
            }
        }
    }
    std::map<string, AnnotationContainer*>::iterator ait;
    for (ait = myAnnotationContainerMap.begin(); ait != myAnnotationContainerMap.end(); ++ait) {
        AnnotationContainer *myAnnotationContainer = (AnnotationContainer*)&(*ait->second);
        if(myAnnotationContainer->m_imageLoader.size() > 0){
            Ref<ImageUtils::LoadImageAsyncTask> task = myAnnotationContainer->m_imageLoader.front();
            if(task->isComplete()){
                Ref<PixelData> pixels = task->getData().image;
                if(pixels == NULL){
                    omsg("======================NULL PIXELS!!=======================");
                    // Failed to load properly, attempt a reload.
                    //myAnnotationContainer->loadImage();
                } else {
                    myAnnotationContainer->imageLoaded(pixels);
                }
                myAnnotationContainer->m_imageLoader.pop_front();
            }
        }
    }
}
void handleEvent(const Event& evt) {
    omsg("HandleEvent");
}
bool handleCommand(const String& cmd) {
    omsg("HandleCommand");
    return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv) {
    Application<ContextuWallApplication> app("ContextuWall");

    return omain(app, argc, argv);
}
