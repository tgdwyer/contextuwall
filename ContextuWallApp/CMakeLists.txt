set(MODULE_NAME ContextuWall)

cmake_minimum_required(VERSION 2.8.1) 
project(${MODULE_NAME})

find_package(Omegalib)

include_directories(${OMEGA_INCLUDE_DIRS})

add_executable(${MODULE_NAME} ContextuWall.cpp)
target_link_libraries(${MODULE_NAME} ${OMEGA_LIB} ${OMEGA_TOOLKIT_LIB})
