using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

public class App : MonoBehaviour {
    public GameObject prefab;

    public Dictionary<string, App.Panel> imageDictionary = new Dictionary<string, App.Panel>();
    public Dictionary<string, App.Annotation> annotationDictionary = new Dictionary<string, App.Annotation>();

    // The index of a panel in this list determines its depth.
    private List<Panel> panels = new List<Panel>();

    void Start() {
        // Put camera in orthographic mode (so we can avoid dealing with view angles).
        Camera.main.orthographic = true;
        Cursor.visible = false;
    }

    void Update() {
        foreach(Panel panel in panels) {
            panel.checkTexturesLoaded();
        }
    }

    public void newPanel(string name, string fullpath, float width, float height, float scale, float x, float y) {
        imageDictionary[name] = new Panel(this, prefab, name, fullpath, width, height, scale, x, y);
    }

    /// <summary>
    /// Refreshes the panel locations.
    /// Useful for updating their depths after a bringToFront/sentToBack call.
    /// </summary>
    private void refreshPanelLocations() {
        foreach(Panel p in panels) {
            p.setLocation(p.m_x, p.m_y);
        }
    }

    public void clearWall() {
        while(panels.Count > 0) {
            panels.First().remove();
        }
        imageDictionary.Clear();
    }

    public class Panel {
        public GameObject imageTextureObj;
        public ImageTexture m_imageTexture;
        public App m_app;

        // The index of a panel in this list determines its depth.
        public List<Annotation> annotations = new List<Annotation>();
        public string m_name, m_url;
        public float m_width, m_height, m_scale, m_x, m_y;
        public Queue<WWW> m_textureLoadQueue = new Queue<WWW>();
        public Material m_material;

        public Panel(App app, GameObject prefab, string name, string fullpath, float width, float height, float scale, float x, float y) {
            m_app = app;
            m_app.panels.Add(this);

            m_name = name;
            m_url = "file://" + fullpath;
            m_width = width;
            m_height = height;

            m_material = new Material(prefab.GetComponent<Renderer>().sharedMaterial);

            imageTextureObj = (GameObject)Instantiate(prefab, new Vector3(0, 0, 0), Quaternion.Euler(270, 0, 0));
            imageTextureObj.name = name;
            m_imageTexture = imageTextureObj.GetComponent<ImageTexture>();
            m_imageTexture.addMaterial(m_material);

            setScale(scale);
            setLocation(x, y);

            loadTexture();
        }

        public void newAnnotation(string name, string fullpath, string ownerID) {
            m_app.annotationDictionary[name] = new Annotation(this, name, fullpath, ownerID);
        }

        public void setLocation(float x, float y) {
            m_x = x;
            m_y = y;

            float pX = 2 * (x - 0.5f) * Camera.main.orthographicSize * Camera.main.aspect;
            float pY = 2 * (0.5f - y) * Camera.main.orthographicSize;
            float pZ = m_app.panels.FindIndex(panel => panel == this) * -0.01f;

            imageTextureObj.transform.position = new Vector3(pX, pY, pZ);

            Debug.Log(string.Format("Moved panel {0} to ({1},{2})=>({3},{4},{5}).", m_name, m_x, m_y, pX, pY, pZ));
        }

        public void loadTexture() {
            print("Image load ordered!");
            m_textureLoadQueue.Enqueue(new WWW(m_url));
        }

        public void checkTexturesLoaded() {
            if(m_textureLoadQueue.Count > 0) {
                WWW www = m_textureLoadQueue.Peek();
                if(www.isDone) {
                    Texture2D.Destroy(m_material.mainTexture);
                    m_material.mainTexture = www.texture;
                    m_textureLoadQueue.Dequeue();
                    www.Dispose();
                    m_imageTexture.updateMaterials();
                }
            }
            foreach(Annotation anno in annotations) {
                anno.checkTextureLoaded();
            }
        }

        public void setScale(float scale) {
            m_scale = scale;
            float pScaleY = (scale == -1) ? m_height / Screen.height : scale;
            float pScaleX = pScaleY * m_width / m_height;

            // We apply the y-scaling to the z-axis, since the object has undergone a 270 degree rotaion about the x-axis.
            imageTextureObj.transform.localScale = new Vector3(pScaleX, 1.0f, pScaleY);
            Debug.Log(string.Format("Scaled panel {0} to {1}=>({2},{3}).", m_name, m_scale, pScaleX, pScaleY));
        }

        public void bringToFront() {
            m_app.panels.Remove(this);
            m_app.panels.Add(this);
            m_app.refreshPanelLocations();
        }

        public void sendToBack() {
            m_app.panels.Remove(this);
            m_app.panels.Insert(0, this);
            m_app.refreshPanelLocations();
        }

        public void remove() {
            m_app.panels.Remove(this);
            m_app.imageDictionary.Remove(this.m_name);
            Destroy(imageTextureObj);
        }

        public override string ToString() {
            return string.Format("name: {0}, dims: ({1},{2}), scale: {3}, pos: ({4}, {5})", m_name, m_width, m_height, m_scale, m_x, m_y);
        }
    }

    public class Annotation {
        public Panel m_panel;
        public string m_name, m_url, m_ownerID;
        public Queue<WWW> m_textureLoadQueue = new Queue<WWW>();
        public Material m_material;

        public Annotation(Panel panel, string name, string fullpath, string ownerID) {
            m_panel = panel;
            m_panel.annotations.Add(this);

            m_name = name;
            m_url = "file://" + fullpath;
            m_ownerID = ownerID;

            m_material = new Material(m_panel.m_app.prefab.GetComponent<Renderer>().sharedMaterial);

            m_panel.m_imageTexture.addMaterial(m_material);

            loadTexture();
        }
        
        public void loadTexture() {
            print("Annotation load ordered!");
            m_textureLoadQueue.Enqueue(new WWW(m_url));
        }

        public void checkTextureLoaded() {
            if(m_textureLoadQueue.Count > 0) {
                WWW www = m_textureLoadQueue.Peek();
                if(www.isDone) {
                    Texture2D.Destroy(m_material.mainTexture);
                    m_material.mainTexture = www.texture;
                    m_textureLoadQueue.Dequeue();
                    www.Dispose();
                    m_panel.m_imageTexture.updateMaterials();
                }
            }
        }
    }
}