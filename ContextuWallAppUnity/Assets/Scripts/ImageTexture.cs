﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;

public class ImageTexture: MonoBehaviour {
    public Renderer m_renderer;
    public List<Material> materials = new List<Material>();

    void Start() {
        m_renderer = GetComponent<Renderer>();
    }

    public void addMaterial(Material material) {
        materials.Add(material);
    }

    /// <summary>
    /// Updates the renderer with local material array.
    /// </summary>
    public void updateMaterials() {
        m_renderer.materials = materials.ToArray();
    }
}