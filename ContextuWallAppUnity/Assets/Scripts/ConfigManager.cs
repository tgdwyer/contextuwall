﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public class ConfigManager: MonoBehaviour {
    Dictionary<string, string> configOptions = new Dictionary<string, string>();

    // Awake() is called before Start(), ensuring these config options will be available to all other scripts on start.
    void Awake() {
        string[] lines;
        try {
            lines = File.ReadAllLines(Directory.GetFiles(Application.dataPath, "config.ini")[0]);
            Debug.Log("Found config file at: " + Application.dataPath + "/config.ini");
        } catch {
            lines = File.ReadAllLines(Directory.GetFiles(Application.dataPath + "/../../Assets", "config.ini")[0]);
            Debug.Log("Found config file at: " + Application.dataPath + "/../../Assets/config.ini");
        }
        foreach(string line in lines) {
            string[] lineArgs = line.Split('=');
            if(lineArgs.Length == 2) {
                configOptions.Add(lineArgs[0], lineArgs[1].Replace(" ", "").Trim());
            }
        }
    }

    public string getConfigOptionString(string option) {
        if(configOptions.ContainsKey(option)) {
            return configOptions[option];
        } else {
            Debug.Log("Cannot get option: " + option);
            return null;
        }
    }

    public int getConfigOptionInt(string option) {
        if(configOptions.ContainsKey(option)) {
            return int.Parse(configOptions[option]);
        } else {
            Debug.Log("Cannot get option: " + option);
            return 0;
        }
    }

    public float getConfigOptionFloat(string option) {
        if(configOptions.ContainsKey(option)) {
            return float.Parse(configOptions[option], System.Globalization.NumberStyles.AllowDecimalPoint);
        } else {
            Debug.Log("Cannot get option: " + option);
            return 0.0f;
        }
    }
}