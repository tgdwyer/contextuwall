﻿using UnityEngine;
using System.Collections;
using System.IO;

public class InboxWatcher : MonoBehaviour {
    public GameObject appObj, commandListenerObj, configManagerObj;

    App m_app;
    CommandListener m_commandListener;

    private FileSystemWatcher m_watcher;
    
    string contextuWallInboxPath;

    void Start() {
        m_app = appObj.GetComponent<App>();
        m_commandListener = commandListenerObj.GetComponent<CommandListener>();
        
        ConfigManager cfg = configManagerObj.GetComponent<ConfigManager>();
        contextuWallInboxPath = cfg.getConfigOptionString("contextuWallInboxPath");
        print(contextuWallInboxPath);

        m_watcher = new FileSystemWatcher(contextuWallInboxPath);
        m_watcher.Changed += OnFileChanged;
        m_watcher.Created += OnFileChanged;

        m_watcher.EnableRaisingEvents = true;
    }

    void OnFileChanged(object sender, FileSystemEventArgs e) {
        string filename = Path.GetFileName(e.Name);
        Debug.Log("\n---inbox file change: " + filename);
        if(m_app.imageDictionary.ContainsKey(filename)) {
            lock(m_commandListener.m_commandLock) {
                m_commandListener.commandQueue.Enqueue("$load-image$" + filename);
            }
        } else if(m_app.annotationDictionary.ContainsKey(filename)) {
            lock(m_commandListener.m_commandLock) {
                m_commandListener.commandQueue.Enqueue("$load-annotation$" + filename);
            }
        }
    }
}