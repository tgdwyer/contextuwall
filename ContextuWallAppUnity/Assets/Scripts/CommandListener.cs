﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;

public class CommandListener: MonoBehaviour {
    public GameObject appObj, configManagerObj;

    App m_app;

    public Queue<string> commandQueue = new Queue<string>();
    public object m_commandLock = new object();

    string commandServerIP;
    int commandServerPort;
    string contextuWallInboxPath;
    string hostname;
    TcpClient tcpclient;

    void Start() {
        m_app = appObj.GetComponent<App>();

        ConfigManager cfg = configManagerObj.GetComponent<ConfigManager>();
        commandServerIP = cfg.getConfigOptionString("commandServerIP");
        commandServerPort = cfg.getConfigOptionInt("commandServerPort");
        hostname = cfg.getConfigOptionString("hostname");
        contextuWallInboxPath = cfg.getConfigOptionString("contextuWallInboxPath");

        connectToServer();
    }

    void Update() {
        string command;
        while(commandQueue.Count > 0) {
            lock(m_commandLock) {
                command = commandQueue.Dequeue();
            }
            handleCommand(command);
        }
    }

    void OnApplicationQuit() {
        if(tcpclient != null) {
            tcpclient.Close();
        }
    }

    void connectToServer() {
        tcpclient = new TcpClient();
        tcpclient.BeginConnect(commandServerIP, commandServerPort, new AsyncCallback(connectedCallback), this);
        Debug.Log(string.Format("Connecting to server at {0}:{1}...", commandServerIP, commandServerPort.ToString()));
    }

    void connectedCallback(IAsyncResult iar) {
        byte[] buffer = new byte[256];
        CommandListener cl = (CommandListener)iar.AsyncState;
        if(cl.tcpclient.Connected) {
            Debug.Log("Connected!");
            NetworkStream tcpStream = (NetworkStream)cl.tcpclient.GetStream();

            byte[] toWrite = Encoding.ASCII.GetBytes(string.Format("$display-specs${0}${1}${2}", hostname, Screen.width, Screen.height));
            tcpStream.Write(toWrite, 0, toWrite.Length);
            while(true) {
                tcpStream.Read(buffer, 0, 4);
                string numBytesHex = new string(Encoding.ASCII.GetChars(buffer, 0, 4));
                int numBytes = int.Parse(numBytesHex, System.Globalization.NumberStyles.HexNumber);

                tcpStream.Read(buffer, 0, numBytes);
                string msg = new string(Encoding.ASCII.GetChars(buffer, 0, numBytes));

                Debug.Log("---incoming Command: " + msg);
                lock(m_commandLock) {
                    commandQueue.Enqueue(msg);
                }
            }
        } else {
            Debug.LogWarning("Failed to connect, trying again in 5s.");
            Thread.Sleep(5000);
            cl.tcpclient.BeginConnect(commandServerIP, commandServerPort, new AsyncCallback(connectedCallback), cl);
        }
    }

    void handleCommand(string command) {
        string[] strs = command.Split('$');
        if(strs.Length < 2) {
            return;
        }

        App.Panel panel;
        string filename, fullpath, ownerID;
        float width, height, scale, x, y;

        switch(strs[1]) {
        case "incoming-image":
            filename = strs[2];
            fullpath = contextuWallInboxPath + filename;
            width = (float)double.Parse(strs[3]);
            height = (float)double.Parse(strs[4]);
            scale = (float)double.Parse(strs[5]);
            x = (float)(double.Parse(strs[6]));
            y = (float)(double.Parse(strs[7]));
            m_app.newPanel(filename, fullpath, width, height, scale, x, y);
            break;

        case "incoming-annotation":
            filename = strs[2];
            if(m_app.imageDictionary.ContainsKey(filename)) {
                panel = m_app.imageDictionary[filename];
                filename = strs[3];
                fullpath = contextuWallInboxPath + filename;
                ownerID = strs[4];
                panel.newAnnotation(filename, fullpath, ownerID);
            }
            break;
            
        case "load-image":
            filename = strs[2];
            if(m_app.imageDictionary.ContainsKey(filename)) {
                panel = m_app.imageDictionary[filename];
                panel.loadTexture();
            }
            break;
            
        case "load-annotation":
            filename = strs[2];
            if(m_app.annotationDictionary.ContainsKey(filename)) {
                m_app.annotationDictionary[filename].loadTexture();
            }
            break;

        case "update-image-location":
            filename = strs[2];
            if(m_app.imageDictionary.ContainsKey(filename)) {
                panel = m_app.imageDictionary[filename];
                x = (float)(double.Parse(strs[3]));
                y = (float)(double.Parse(strs[4]));
                panel.setLocation(x, y);
            }
            break;

        case "resize-image":
            filename = strs[2];
            if(m_app.imageDictionary.ContainsKey(filename)) {
                panel = m_app.imageDictionary[filename];
                scale = (float)double.Parse(strs[3]);
                panel.setScale(scale);
            }
            break;

        case "bring-image-to-front":
            filename = strs[2];
            if(m_app.imageDictionary.ContainsKey(filename)) {
                panel = m_app.imageDictionary[filename];
                panel.bringToFront();
            }
            break;

        case "send-image-to-back":
            filename = strs[2];
            if(m_app.imageDictionary.ContainsKey(filename)) {
                panel = m_app.imageDictionary[filename];
                panel.sendToBack();
            }
            break;

        case "remove-image":
            filename = strs[2];
            if(m_app.imageDictionary.ContainsKey(filename)) {
                panel = m_app.imageDictionary[filename];
                panel.remove();
            }
            break;

        case "clear-wall":
            m_app.clearWall();
            break;

        default:
            Debug.LogWarning("---ERROR: Unknown command: " + command);
            break;
        }
    }
}